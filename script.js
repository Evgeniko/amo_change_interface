define(['jquery'], function ($) {
	var CustomWidget = function () {
		var self = this;
		var subdomain = AMOCRM.constant('account').subdomain;
		// Функция скрытия элементов для менеджеров
		this.hideElems = function () {
			// Скрытие пунктов меню из сделки
			var menu = $('#nav_menu');
			var elements = ['dashboard', 'stats', 'settings', 'mail', 'catalogs'];
			elements.forEach(function (elem) {
				$(menu).find('div[data-entity="' + elem + '"]').hide();
			});
			// Скрываем бюджет сделки
			$('#lead_card_budget').closest('.linked-form__field').hide();
			// Скрываем номера телефонов из сделки
			$('.control-phone').css('display', 'none');
		};
		// Изменнение элементов страницы для ВСЕХ ролей
		this.renameElems = function () {
			var menu = $('#nav_menu');
			$(menu).find('div[data-entity="leads"]').find('div.nav__menu__item__title').html('Проекты');
			$('span.pipeline_status__head_info__sum').hide();
			$('span.pipeline_status__head_info__count').html(function (index, oldhtml) {
				return oldhtml.replace(':', '');
			});
			var target = $('#left_menu');
			if (target.length > 0) {
				var observer = new MutationObserver(function (mutations) {
					mutations.forEach(function (mutation) {
						var nodes = $(mutation.addedNodes).find('.list-multiple-actions__actions-wrapper');
						if ($(nodes).html() === 'Сделки') {
							$(nodes).html('Проекты');
						}
					});
				});
				var config = { attributes: true, childList: true, characterData: true };
				observer.observe(target[0], config);
			}
			$('.conversion-chart__status-budget').hide();
		};
		// Вычисляем процент на рабочем столе
		this.conversionRewrite = function () {
			var targ = $('body');
			if (targ.length > 0) {
				var convers = new MutationObserver(function (mutations) {
					mutations.forEach(function (mutation) {
					// setTimeout(function(){
						var counts = $('.leads-transfer__inner');
						if (counts.length > 0) {
							for (var i = counts.length - 1; i > 0; i--) {
								$(counts[i]).find('.leads-transfer__percentage-value').html(function (index, oldhtml) {
									var cur = parseInt($(counts[i]).find('.leads-transfer__count-value').html().replace(/\D+/g, ''));
									var bef = parseInt($(counts[i - 1]).find('.leads-transfer__count-value').html().replace(/\D+/g, ''));
									if ((cur > 0) && (bef === 0)) {
										return '∞';
									} else if ((cur === 0) && (bef > 0)) {
										return '0 %';
									} else if ((cur === 0) && (bef === 0)) {
										return '0 %';
									} else {
										return cur / bef * 100 + ' %';
									}
								});
							}
							$(counts[0]).find('.leads-transfer__percentage-value').html('100%');
							$('.conversion-chart__status-budget').hide();
						}
					// }, 500);
					});
				});
				var config = { attributes: true, childList: true, characterData: true };
				convers.observe(targ[0], config);
			}
		};
		this.oldLeads = function () {
			var leadsId = [];
			$('.pipeline_leads__item').each(function (i, elem) {
				leadsId.push($(elem).attr('data-id'));
			});
			var get =  function (params = {}) {
				var response = [];
				var offset = 0;
				var request = function () {
					params.type = 'lead';
					var result = false;
					$.ajax({
						url: 'https://' + subdomain + '.amocrm.ru/api/v2/notes',
						async: false,
						type: 'GET',
						dataType: 'json',
						data: {
							type: 'lead',
							limit_rows: 500,
							limit_offset: offset,
							element_id: leadsId,
							note_type: [1, 3] // Примечания именно по изменению статуса
						}
					})
					.done(function (data) {
						result = (data._embedded.items) ? data._embedded.items : false;
					});
					return result;
				};
				function compound () {
					var part = request();
					if (part) {
						response = response.concat(part);
						if (part.length >= 500) {
							offset += part.length;
							compound();
						}
					}
				};
				compound();
				return response;
			};
			var notes = get();
			var results = {};
			leadsId.forEach(function (item, i, arr) {
				for (var key in notes) {
					if (parseInt(item) === parseInt(notes[key].element_id)) {
						results[item] = notes[key].created_at;
					}
				}
				for (var k in results) {
					var difference = parseInt(new Date().getTime() / 1000) - parseInt(results[k]);
					if (difference > 7 * 24 * 60 * 60) {
						$('[data-id="' + k + '"]').css('box-shadow', '0 0 1px 2px red');
					}
				}
			});
		};

		this.callbacks = {
			render: function () {
				$(document).ready(function () {
					var userId = AMOCRM.constant('user').id;
					$.get('https://' + subdomain + '.amocrm.ru/api/v2/account?with=users', {}, function (data) {
						if (!data._embedded.users[userId].is_admin) {
							self.hideElems();
						}
					});
					self.renameElems();
					self.conversionRewrite();
					self.oldLeads();
					$(document).ajaxSuccess(function (event, xhr, settings) {
						var urlLoad = '/ajax/leads/multiple/loadmore/';
						if (settings.url.indexOf(urlLoad) + 1) {
							self.oldLeads();
						}
					});
				});
				return true;
			},
			init: function () {
				return true;
			},
			bind_actions: function () {
				return true;
			},
			settings: function () {
				return true;
			},
			onSave: function () {
				return true;
			},
			destroy: function () {
			},
			contacts: {
				// select contacts in list and clicked on widget name
				selected: function () {
					console.log('contacts');
				}
			},
			leads: {
				// select leads in list and clicked on widget name
				selected: function () {
					console.log('leads');
				}
			},
			tasks: {
				// select taks in list and clicked on widget name
				selected: function () {
					console.log('tasks');
				}
			}
		};
		return this;
	};

	return CustomWidget;
});
